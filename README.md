# Optus Jasmine Testing

Yep, testing Jasmine, Grunt, Node, jQuery.

## Requirements

node.js
git

## Install

- git clone git@bitbucket.org:scottos/optus-jasmine-testing.git
- sudo npm install -g grunt
- npm install

## Run

- node server.js
- http://localhost:3000
- http://localhost:3000/jasmine/SpecRunner.html
